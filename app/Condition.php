<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    const SOCIALS = [
        0b00000001 => 'google',
        0b00000010 => 'facebook',
        0b00000100 => 'twitter',
        0b00001000 => 'instagram'
    ];

    protected $fillable = [
        'name',
        'token',
        'country',
        'browser',
        'platform',
        'accept_language',
        'socials'
    ];

    public function tasks(){
        return $this->belongsToMany(Task::class);
    }

    public function toArray()
    {
        $attributes = $this->attributesToArray();

        foreach (static::SOCIALS as $key => $name)
            $attributes[$name] = $this->socials & $key;

        return array_merge($attributes, $this->relationsToArray());
    }
}
