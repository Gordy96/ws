<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'hash', 'task_id', 'active', 'country', 'browser', 'platform', 'accept_language', 'socials', 'session_start', 'session_end'
    ];

    protected $casts = [
        'socials'=>'object'
    ];

    public function task(){
        return $this->belongsTo(Task::class);
    }
    public function getSessionDurationAttribute() {
        $se = $this->session_end;
        $ss = $this->session_start;
        if(!$this->active && !$se)
            return 0;
        if(!$se)
            $now = \DateTime::createFromFormat("Y-m-d H:i:s", date("Y-m-d H:i:s"));
        else
            $now = \DateTime::createFromFormat("Y-m-d H:i:s", $se);
        $dif = $now->diff(\DateTime::createFromFormat("Y-m-d H:i:s", $ss));
        return $dif->format("%H:%I:%S");
    }
    public function finishSession($d = null){
        $this->session_end = \DateTime::createFromFormat("Y-m-d H:i:s", $d ? $d : date("Y-m-d H:i:s"));
    }
}
