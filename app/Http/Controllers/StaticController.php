<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StaticController extends Controller
{
    public function getFingerPrintScript(Request $request){
        return response(view('admin.fp')->render(), 200,
            [
                "content-type"=>"application/javascript"
            ]
        );
    }

    public function getEmbedScript(Request $request){
        $token = $request->get('token',"0");
        $address = $request->has('url') ? $request->get('url') : "";
        return response(view('admin.fp')->render() . view('admin.script', ['token'=>$token, 'address'=>$address, 'embedded'=>true])->render(), 200,
            [
                "content-type"=>"application/javascript"
            ]
        );
    }
}
