<?php

namespace App\Http\Controllers;

use App\Sockets\Test;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

use App\Condition;
use App\Task;

use App\Jobs\AbandonTask;
use Mockery\Exception;

class AdminController extends Controller
{
    public function conditions(Request $request){
        return view('admin.conditions', ["config"=>config('condition')]);
    }
    public function apiGetCondition(Request $request){
        $c = Condition::find($request->get('con'));
        if($c){
            return response()->json($c->toArray());
        }
        else
            return response('error', 500);
    }
    public function apiSetCondition(Request $request){
        $res = ['type'=>'info'];
        try{
            $credentials = [
                'name'=>$request->get('name'),
                'token'=>$request->get('token'),
                'country'=>$request->get('country'),
                'browser'=>$request->get('browser'),
                'platform'=>$request->get('platform'),
                'accept_language'=>$request->get('accept_language')
            ];
            $credentials['socials'] = 0;
            $soc = array_flip(Condition::SOCIALS);
            if($request->has('google')){
                $credentials['socials'] = $credentials['socials'] | $soc['google'];
            }
            if($request->has('twitter')){
                $credentials['socials'] = $credentials['socials'] | $soc['twitter'];
            }
            if($request->has('facebook')){
                $credentials['socials'] = $credentials['socials'] | $soc['facebook'];
            }
            if($request->has('instagram')){
                $credentials['socials'] = $credentials['socials'] | $soc['instagram'];
            }
            if($request->has('id') && $request->get('id')){
                $condition = Condition::where('id', $request->get('id'))->update($credentials);
                if(!$condition){
                    $res["error"] = "Cannot update entry";
                    $res["type"] = 'danger';
                }
            }
            else{
                $condition = Condition::create($credentials);
                if(!$condition){
                    $res["error"] = "Cannot save entry";
                    $res["type"] = 'danger';
                }else
                    $res["data"] = ["id"=>$condition->id,"name"=>$condition->name];
            }
        }
        catch(\Illuminate\Database\QueryException $exception){
            $res = self::ParseSqlError($exception);
        }
        return response()->json($res);
    }

    public function tasks(Request $request){
        return view('admin.tasks', ["config"=>config('condition')]);
    }

    public function connections(Request $request){
        return view('admin.connections');
    }

    public function apiGetTask(Request $request){
        $c = Task::with("conditions")->find($request->get('task'));
        if($c){
            return response()->json($c->toArray());
        }
        else
            return response('error', 500);
    }

    public function apiSetTask(Request $request){
        $res = ['type'=>'info'];
        try{
            $scr = $request->get('script');
            $credentials = [
                'name'=>$request->get('name'),
                'script'=>$scr
            ];
            if($request->has('id') && $request->get('id')){
                $task = Task::find($request->get('id'));
                $condition = $task->update($credentials);
                $task->save();
                dispatch(new AbandonTask($task))->onQueue('tasks');
                if(!$condition){
                    $res["error"] = "Cannot update entry";
                    $res["type"] = 'danger';
                }
            }
            else{
                $condition = Task::create($credentials);
                if(!$condition){
                    $res["error"] = "Cannot save entry";
                    $res["type"] = 'danger';
                }else
                    $res["data"] = ["id"=>$condition->id,"name"=>$condition->name];
            }
        }
        catch(\Illuminate\Database\QueryException $exception){
            $res = self::ParseSqlError($exception);
        }
        return response()->json($res);
    }

    public function delete(Request $request, $subject = null, $id = 0){
        if (is_null($subject)) return response()->json("fuck you");
        $res = ['type'=>'info'];
        $callable = null;
        $c = null;
        switch ($subject){
            case "condition":
                $c = \App\Condition::find($id);
                if(!$c) return response()->json("fuck you");
                $callable = "tasks";
                break;
            case "task":
                $c = \App\Task::find($id);
                $callable = "conditions";
                break;
        }
        if(!$c || !$callable){
            $res["error"] = "Cannot update entry";
            $res["type"] = 'danger';
            return response()->json($res);
        }
        $c->{$callable}()->sync([]);
        try{
            $c->delete();
        }
        catch (\Illuminate\Database\QueryException $e) {
            $res = self::ParseSqlError($e);
        }
        return response()->json($res);
    }

    public function syncTaskCondition(Request $request){
        $task = Task::find($request->get('id'));
        dispatch(new AbandonTask($task))->onQueue('tasks');
        $result = $task->conditions()->sync($request->get('conditions'));
        $res = ['type'=>'info'];
        if(!$result)
            $res['type'] = 'danger';
        return response()->json($res);
    }

    public function resetTaskCondition(Request $request){
        $tasks = Task::all();

        foreach($tasks as $task){
            dispatch(new AbandonTask($task))->onQueue('tasks');
            $result = $task->conditions()->sync([]);
        }
        $res = ['type'=>'info'];
        if(!$result)
            $res['type'] = 'danger';
        return response()->json($res);
    }

    private static function ParseSqlError(\Illuminate\Database\QueryException $exception){
        $res = [];
        $res["type"] = 'danger';
        $res['error'] = 'Unknown error';
        if(strpos($exception->getMessage(), 'unique')){
            $m = [];
            preg_match_all("/.*\s'.*_([^\s]+)_unique/",$exception->getMessage(), $m);
            $res["error"] = "Duplicate on " . $m[1][0];
        } else if (strpos($exception->getMessage(), '1451')){
            $m = [];
            preg_match_all("/\\.`(.*)`, CONSTRAINT/",$exception->getMessage(), $m);
            $res["error"] = "There are still dependencies in " . $m[1][0];
        }
        else{
            $res['error'] = $exception->getMessage();
        }
        return $res;
    }

    public function export(){
        return view('admin.export');
    }
    public function statistics(){
        return view('admin.statistics');
    }
    public function clients($active = null){
        if(!$active)
            $clients = \App\Client::with('task')->get();
        else
            $clients = \App\Client::with('task')->where('active', true)->get();
        return view('admin.clients', ['clients'=>$clients, 'active'=>$active]);
    }
    public function frames(Request $request) {
        if($request->isMethod('get'))
            return view('admin.frames');

        session()->put('frame', ["address"=>$request->get('address'),"code"=>$request->get('code')]);
        return response()->json(["status"=>true,"address"=>$request->get('address')]);
    }
    public function frame(Request $request) {
        $data = session()->get('frame');
        return view('admin.frame', $data);
    }
}
