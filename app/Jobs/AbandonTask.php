<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Task;

class AbandonTask implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $hashes;

    public function __construct($task)
    {
        $this->hashes = [];
        foreach($task->clients as $client){
            array_push($this->hashes, $client->hash);
        }
    }

    /**
     * Execute the job.
     * @param \SplObjectStorage $connections
     * @return void
     */
    public function handle(\SplObjectStorage $connections)
    {
        foreach ($connections as $connection){
            $c = $connections->current();
            $hash = $connections->getInfo();
            if(in_array($hash, $this->hashes))
                $c->close(1010);
        }
    }
}
