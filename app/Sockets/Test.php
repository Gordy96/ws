<?php
namespace App\Sockets;
use GeoIp2\Exception\GeoIp2Exception;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use GuzzleHttp\Psr7\Request;
use GeoIp2\Database\Reader;

use App\Condition;
use App\Task;
use App\Variable;
use App\Client;
use App\Jobs\AbandonTask;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Queue;

class Test implements MessageComponentInterface {
    protected $clients;
    private $logger;

    public function __construct(LoggerInterface $log) {
        $this->clients = new \SplObjectStorage;
        $this->logger = $log;
    }

    public function loopCall($loop){
        $job = app('queue')->pop('tasks');
        if($job){
            $handler = unserialize($job->payload()['data']['command']);
            $handler->handle($this->clients);
            $job->delete();
        }
        Variable::VarSet('connections', $this->clients->count());
        $loop->futureTick([$this, 'loopCall']);
    }

    private static function tof($str){
        return str_ireplace(" ", "_", strtolower($str));
    }

    public function onOpen(ConnectionInterface $conn) {
        $request = $conn->httpRequest;

        $al = $request->getHeader('Accept-Language')[0];
        $ua = $request->getHeader('User-Agent')[0];

        $ip = $conn->remoteAddress;
        $ri = $conn->resourceId;

        Variable::incrementConnections();
        $reader = new Reader(base_path('resources/other/geoip.mmdb'));
        try{
            $record = $reader->country($ip);
            $country = $record->country->isoCode;
        }
        catch(GeoIp2Exception $e){
            $country = config('condition.defaults.country');
        }

        $credentials = parse_user_agent($ua);
        $task = null;
        $token = null;
        $uri = explode("/", substr($request->getRequestTarget(), 1));
        $this->logger->log($request->getRequestTarget());
        if(count($uri) > 0 && strlen($uri[0])){
            $hash = $uri[0];
            $this->logger->log("hash: $hash");

            $client = Client::where('hash',$hash)->first();

            if(count($uri) > 1 && strlen($uri[1]))
                $token = $uri[1];
            $socials = [];
            $pres = [];
            if(count($uri) > 2 && strlen($uri[2])){
                $soc_flag = intval($uri[2]);
                $names = array_flip(Condition::SOCIALS);
                foreach($names as $name => $mask){
                    if($soc_flag & $mask){
                        array_push($pres, $mask);
                        array_push($socials, $name);
                    }
                }
            }

            if(!$client){
                $client = Client::create([
                    'hash'=>$hash,
                    'country'=>$country,
                    'browser'=>$credentials['browser'],
                    'platform'=>$credentials['platform'],
                    'accept_language'=>$al,
                    'socials'=>$socials,
                    'session_start'=>date('Y-m-d H:i:s')
                ]);
            }
        }
        else{
            $conn->close();
            return;
        }

        if($token && $token !== "0"){
            $task = Condition::where('token', $token)->with('tasks')->get();
            if(!count($task))
                $task = null;
        }
        if($task === null){
            $bindings = [
                "c"=>$country,
                "cs"=>config('condition.weights.country'),
                "u"=>self::tof($credentials['platform']),
                "us"=>config('condition.weights.platform'),
                "m"=>self::tof($credentials['browser']),
                "ms"=>config('condition.weights.browser'),
                "a"=>substr($al, 0, strpos($al, ",") > 0 ? strpos($al, ",") : strlen($al)),
                "as"=>config('condition.weights.language')
            ];
            $soc_order_string = "";
            for($i = 0; $i < count($pres); $i++){
                $bindings['s'.$i] = $pres[$i];
                $bindings["ss".$i] = config('condition.weights.socials');
                $soc_order_string .= "+if(socials & :s{$i}, :ss{$i}, 0)";
            }
            $this->logger->log(json_encode($bindings));
            $task = Condition::orderByRaw(
                "(
                if(country = :c or country = '*', :cs, 0) + 
                if(platform = :u or platform = '*', :us, 0) + 
                if(browser = :m or browser = '*', :ms, 0) + 
                if(instr(:a, accept_language) or accept_language = '*', :as, 0)
                ".$soc_order_string."
                ) 
                DESC",
                $bindings
                )->with('tasks');
            $this->logger->log($task->toSql());
            $this->logger->log($task->toSql());
            $task = $task->get();
        }
        if(count($task)){
            $cl = true;
            foreach($task as $c){
                if(count($c->tasks)){
                    $t = $c->tasks[0];
                    $this->clients->attach($conn, $hash);
                    $client->task()->associate($t);
                    $client->active = true;
                    $client->country = $country;
                    $client->browser = $credentials['browser'];
                    $client->platform = $credentials['platform'];
                    $client->accept_language = $al;
                    $d = date('Y-m-d H:i:s');
                    echo "updating session for ({$hash}) ($d)\n";
                    $client->session_start = $d;
                    $client->session_end = null;

                    $client->save();
                    $conn->send($t->script);
                    $cl = false;
                    echo "New connection! ({$conn->resourceId})\n";
                }
            }
            if($cl){
                $conn->close(1010);
            }
        }
        else{
            $conn->close(1010);
        }
    }

    public function Broadcast($message, $to = null){
        if(is_null($to)){
            foreach ($this->clients as $client) {
                $client->send($message);
            }
        }elseif(is_array($to)){
            foreach ($this->clients as $client) {
                if(in_array($client->resourceId, $to))
                    $client->send($message);
            }
        }
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $ojb = @json_decode($msg);
        if($ojb !== null){

        }
    }

    public function onClose(ConnectionInterface $conn) {
        $hash = $this->clients->offsetGet($conn);
        $client = Client::where('hash', $hash)->first();
        $client->active=false;
        $client->finishSession();
        $client->save();
        $this->clients->detach($conn);
        Variable::decrementConnections();

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}