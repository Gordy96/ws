<?php
/**
 * Created by PhpStorm.
 * User: johnny
 * Date: 11.05.18
 * Time: 11:55
 */

namespace App\Sockets;


interface LoggerInterface
{
    public function log($text);
}