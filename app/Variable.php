<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Variable extends Model
{
    protected $fillable = [
        'name', 'value'
    ];

    public static function VarSet($name, $value){
        $temp = self::where('name', $name)->update([
            'value'=>$value
        ]);

    }
    public static function VarGet($name){
        $temp = self::where('name', $name)->first();
        return $temp->value;
    }
    public static function incrementConnections(){
        self::where('name', 'connections')->increment('value');
    }
    public static function decrementConnections(){
        self::where('name', 'connections')->decrement('value');
    }
}
