<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'name',
        'script'
    ];

    public function conditions(){
        return $this->belongsToMany(Condition::class);
    }

    public function clients(){
        return $this->hasMany(Client::class);
    }
}
