<?php

namespace App\Console\Commands;

use App\Sockets\LoggerInterface;
use Illuminate\Console\Command;

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use App\Sockets\Test;


class RunWebSocketServer extends Command implements LoggerInterface
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wsserver:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs a dedicated websocket server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $accessor = new Test($this);
        
            $server = IoServer::factory(
                new HttpServer(
                    new WsServer(
                        $accessor
                    )
                ),
                3131
            );
        
            \App\Client::where('active', true)->update(['active'=>false]);
        
            $server->loop->futureTick([$accessor, 'loopCall']);
            $server->run();
    }

    public function log($text){
        $this->info($text);
    }
}
