<?php
Route::group(['middleware'=>['auth']],function() {
    Route::get('js/{name}', function ($name = null) {
        if (file_exists(base_path("private/js/$name"))) {
            $contents = file_get_contents(base_path("private/js/$name"));
            $response = Response::make($contents, 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;
        } else
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Not found');
    })->where('name', '(.*)');

    Route::group(['prefix'=>'control'],function(){
        Route::group(['prefix'=>'api'],function(){
            Route::group(['prefix'=>'get'],function(){
                Route::any('condition', 'AdminController@apiGetCondition');
                Route::any('task', 'AdminController@apiGetTask');
            });
            Route::group(['prefix'=>'set'],function(){
                Route::any('condition', 'AdminController@apiSetCondition');
                Route::any('task', 'AdminController@apiSetTask');
            });
            Route::group(['prefix'=>'sync'], function(){
                Route::any('task/condition', 'AdminController@syncTaskCondition');
            });
            Route::group(['prefix'=>'reset'], function(){
                Route::any('connections', 'AdminController@resetTaskCondition');
            });
            Route::group(['prefix'=>'delete'], function(){
                Route::any('/{subject}/{id}', 'AdminController@delete');
            });
        });
        Route::get('conditions', 'AdminController@conditions');
        Route::get('connections', 'AdminController@connections');
        Route::get('tasks', 'AdminController@tasks');
        Route::get('export', 'AdminController@export');
        Route::get('statistics', 'AdminController@statistics');
        Route::get('clients/{active?}', 'AdminController@clients');
        Route::any('frames', 'AdminController@frames');
        Route::any('frame', 'AdminController@frame');
    });
});

Route::any('/login', function (\Illuminate\Http\Request $request) {
    if(Auth::user())
        return redirect('/control/conditions');
    if($request->isMethod('post')){
        if(Auth::attempt([
            "email"=>$request->get('email'),
            "password"=>$request->get('password')
        ])){
            return redirect()->intended();
        }
        else
            return redirect()->back()->withInput($request->all());
    }
    else
        return view('admin.login');
})->name('login');

Route::get('logout', function(){
    Auth::logout();
    return redirect('/login');
});

Route::get('/fp', 'StaticController@getFingerPrintScript');
Route::get('/utils.js', 'StaticController@getEmbedScript');

Route::get('/{token?}', function (\Illuminate\Http\Request $request, $token = null) {
    if(Auth::user())
        return redirect('/control/conditions');
    if(!$token)
        $token = "0";
    $address = $request->has('url') ? $request->get('url') : "";

    return view('admin.frame', ["token"=>$token, "address"=>$address]);
});