@extends('layout')
@section('body')
    <p>Add new condition</p>
    <form action="/add/condition" method="post">
        <input name="name" type="text" placeholder="name">
        <input name="country" type="text" placeholder="country">
        <input name="platform" type="text" placeholder="platform">
        <input name="browser" type="text" placeholder="browser">
        <input name="language" type="text" placeholder="language">
        <input name="token" type="text" placeholder="token">
        <input type="submit">
    </form>
    <p>Edit condition</p>
    <form action="/control" method="post">
        <select name="condition">
            @foreach(\App\Condition::all() as $condition)
                <option value="{{$condition->id}}">{{$condition->name}}</option>
            @endforeach
        </select>
        <input type="submit">
    </form>
    @if(isset($con))
    <form action="/set/condition" method="post">
        <input type="hidden" name="id" value="{{$con->id}}">
        <input name="name" type="text" placeholder="name" value="{{$con->name}}">
        <input name="country" type="text" placeholder="country" value="{{$con->country}}">
        <input name="platform" type="text" placeholder="platform" value="{{$con->platform}}">
        <input name="browser" type="text" placeholder="browser" value="{{$con->browser}}">
        <input name="language" type="text" placeholder="language" value="{{$con->accept_language}}">
        <input name="token" type="text" placeholder="token" value="{{$con->token}}">
        <input type="submit">
    </form>
    @endif
    <p>Add new task</p>
    <form action="/add/task" method="post">
        <input name="name" type="text" placeholder="name">
        <textarea name="script" placeholder="script"></textarea>
        <input type="submit">
    </form>
    <p>Task-Condition</p>
    <form action="/add/task/condition" method="post">
        <select name="task">
            @foreach($tasks as $task)
                <option value="{{$task->id}}">{{$task->name}}</option>
            @endforeach
        </select>
        <select name="conditions" multiple>
            @foreach($conditions as $condition)
                <option value="{{$condition->id}}">{{$condition->name}}</option>
            @endforeach
        </select>
        <input type="submit">
    </form>
    <p>Active connections: {{\App\Variable::VarGet('connections')}}</p>
@endsection