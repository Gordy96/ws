@extends('admin.layout')
@section('body')
    <div class="notifications" style="position:fixed;top:0px;right:0px;max-width:600px;z-index:99"></div>
    <div class="panel">
        <p class="panel-heading">
            Task:
        </p>
        <div class="panel-block">
            <div class="control">
                <div class="field">
                    <label class="label">Task for editing</label>
                </div>
                <div class="field has-addons">
                    <div class="control is-expanded">
                        <div class="is-fullwidth">
                            <input type="text" name="task" class="input" list="tasks"/>
                            <datalist id="tasks">
                                <label>
                                    <select name="task">
                                        @foreach(\App\Task::all(['id','name']) as $task)
                                            <option value="{{$task->id}}">{{$task->name}}</option>
                                        @endforeach
                                    </select>
                                </label>
                            </datalist>
                        </div>
                    </div>
                    <div class="control">
                        <button type="submit" id="edit" class="button is-primary">Edit</button>
                        <button type="submit" id="delete" class="button is-primary">Delete</button>
                        <button type="submit" id="clear" class="button is-primary">Clear</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-block">
            <form method="post" action="/control/api/set/task" id="main" class="control">
                <div class="field">
                    <label class="label">Name</label>
                    <div class="control">
                        <input name="name" class="input" type="text">
                    </div>
                </div>
                <div class="field">
                    <div class="control is-expanded">
                        <label class="label">Script</label>
                        <input name="script" class="input" type="hidden">
                        <div style="min-height: 500px">
                            <pre id="editor" style="height: 500px;">
function foo() {
    var x = "All this is syntax highlighted";
    return x;
}
console.log(foo());
                            </pre>
                        </div>
                    </div>
                </div>
                <input name="id" class="input" type="hidden">
                <div class="field is-grouped">
                    <div class="control">
                        <input class="button is-primary" id="confirm" value="Create" type="submit">
                    </div>
                    <div class="control">
                        <label class="checkbox has-text-centered">
                            <input type="checkbox" id="minify">
                            minify?
                        </label>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('js/ace/ace.js')}}" type="text/javascript" charset="utf-8"></script>
    <script src="{{asset('js/lib/utils.js')}}"></script>
    <script src="{{asset('js/lib/ast.js')}}"></script>
    <script src="{{asset('js/lib/parse.js')}}"></script>
    <script src="{{asset('js/lib/transform.js')}}"></script>
    <script src="{{asset('js/lib/scope.js')}}"></script>
    <script src="{{asset('js/lib/output.js')}}"></script>
    <script src="{{asset('js/lib/compress.js')}}"></script>
    <script src="{{asset('js/lib/beautify.js')}}"></script>
    <script>
        var editor = ace.edit("editor");
        editor.setTheme("ace/theme/monokai");
        editor.getSession().setMode("ace/mode/javascript");
        editor.getSession().on('change', function(ed){
            $('input[name=script]').val(editor.getValue());
        });
        $(document).ready(function(){
            $('#delete').on('click', function(){
                var val = $('input[name=task]').val();
                if(val.length){
                    axios.get('/control/api/delete/task/' + val, {}).then(function(response){
                        appendNotification(response.data.type, response.data.error ? 'Error' : 'Ok', response.data.error);
                        if(!response.data.error){
                            $('select[name=condition] > option[value=' + val + ']').remove();
                            $('#clear').click();
                        }
                    }).catch(function(e){
                        appendNotification('danger', 'Error', 'No such entry');
                    });
                }
            });
            $('#edit').on('click', function(){
                var val = $('input[name=task]').val();
                if(val.length){
                    axios.get('/control/api/get/task', {
                        params:{
                            task: val
                        }
                    }).then(function(response){
                        Object.keys(response.data).forEach(function(element){
                            var elem = $('input[name=' + element + ']');
                            if(elem.length)
                                elem.val(response.data[element]);
                        });
                        editor.setValue(js_beautify(response.data.script));
                        $("#confirm").val('Update');
                    }).catch(function(e){
                        appendNotification('danger', 'Error', 'No such entry');
                    });
                }
            });
            $('input[name=task]').on('input',function(){
                var val = $('input[name=task]').val();
                if(!val.length){
                    $('input[name]').val('');
                    $("#confirm").val('Create');
                }
            });
            $('#clear').on('click',function(){
                $('input[name]').val('');
                $("#confirm").val('Create');
                editor.setValue('');
            });
            $('#main').submit(function(){
                var fields = {};
                $(this).find('input[name]').each(function(){
                    fields[this.name] = this.value;
                });
                fields.script = editor.getValue();
                if($('#minify:checked').length && fields.script.length)
                    fields.script = window.uglify(fields.script, {});
                axios.post(this.action, fields).then(function(response){
                    appendNotification(response.data.type, response.data.error ? 'Error' : 'Ok', response.data.error);
                    if(!response.data.error && response.data.data){
                        Object.keys(response.data.data).forEach(function(element, index){
                            var node = $('input[name=' + element + ']');
                            if(node.length)
                                node.val(response.data.data[element]);
                        });
                        $('select[name=task]').append('<option value="' + response.data.data.id + '">' + response.data.data.name + '</option>')
                    }
                });
                return false;
            });
        });
    </script>
@endsection