<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body style="margin:0;padding:0;border:none">
    <script type="application/javascript" src="{{url('/utils.js')}}?url={{$address}}"></script>
    {{--<script>--}}
        {{--@include('admin.script')--}}
    {{--</script>--}}
</body>
</html>