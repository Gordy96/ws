@extends('admin.layout')
@section('body')
    <div class="notifications" style="position:fixed;top:0px;right:0px;max-width:600px;z-index:99"></div>
    <div class="panel">
        <div class="panel-block">
            <div class="control">
                <div class="field">
                    <label class="label">Dependencies</label>
                    <pre>&lt;script type="application/javascript" src="{{asset('fp')}}"&gt;&lt;/script&gt;</pre>
                    <h2>Or download it:</h2>
                    <a href="{{asset('fp')}}" download="fingerprint2.js">FingerprintJS2</a>
                </div>
                <div class="field">
                    <div class="control">
                        <label class="label">Script</label>
                        <div class="alert alert-warning">Replace <strong>{token}</strong> and <strong>{address}</strong> in code for yours
                            <div>
                                <div class="field is-grouped">
                                    <div class="control">
                                        <label class="checkbox has-text-centered">
                                            <input type="text" id="token" placeholder="token" value="">
                                        </label>
                                    </div>
                                    <div class="control">
                                        <label class="checkbox has-text-centered">
                                            <input type="text" id="address" placeholder="address" value="">
                                        </label>
                                    </div>
                                    <div class="control">
                                        <label class="checkbox has-text-centered">
                                            <input type="button" id="replace" value="replace">
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="alert alert-danger">Or just add this to you html, but first <strong>REPLACE {address}</strong>
                            <pre id="script_utils">
&lt;script type="application/javascript" src="{{url('/utils.js')}}?url={address}"&gt;&lt;/script&gt;
                            </pre>
                            <div>
                                <div class="field is-grouped">
                                    <div class="control">
                                        <label class="checkbox has-text-centered">
                                            <input type="text" id="address2" placeholder="address" value="">
                                        </label>
                                    </div>
                                    <div class="control">
                                        <label class="checkbox has-text-centered">
                                            <input type="button" id="replace2" value="replace">
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input name="script" class="input" type="hidden">
                        <div style="min-height: 500px; max-width: 600px">
                            <pre id="editor" style="height: 500px;">
@include('admin.script', ['address'=>'{{address}}','token'=>'{{token}}'])
                            </pre>
                        </div>
                    </div>
                </div>
                <div class="field is-grouped">
                    <div class="control">
                        <label class="checkbox has-text-centered">
                            <input type="button" id="default" value="restore">
                        </label>
                    </div>
                    <div class="control">
                        <label class="checkbox has-text-centered">
                            <input type="checkbox" id="minify">
                            minify?
                        </label>
                    </div>
                    <div class="control">
                        <textarea style="display:none" id="fp">
@include('admin.fp')
                        </textarea>
                        <label class="checkbox has-text-centered">
                            <input type="checkbox" id="nodeps">
                            no dependencies
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('js/ace/ace.js')}}" type="text/javascript" charset="utf-8"></script>
    <script src="{{asset('js/lib/utils.js')}}"></script>
    <script src="{{asset('js/lib/ast.js')}}"></script>
    <script src="{{asset('js/lib/parse.js')}}"></script>
    <script src="{{asset('js/lib/transform.js')}}"></script>
    <script src="{{asset('js/lib/scope.js')}}"></script>
    <script src="{{asset('js/lib/output.js')}}"></script>
    <script src="{{asset('js/lib/compress.js')}}"></script>
    <script src="{{asset('js/lib/beautify.js')}}"></script>
    <script>
        var editor = ace.edit("editor");
        editor.setTheme("ace/theme/monokai");
        editor.getSession().setMode("ace/mode/javascript");
        var oldval = editor.getValue();
        var fp2 = $('#fp').val();
        var fpload = "";
        editor.getSession().on('change', function(ed){
            $('input[name=script]').val(editor.getValue());
        });
        editor.setValue(oldval + '\n' + fpload);
        var tval = editor.getValue();
        $(document).ready(function(){
            $('#minify').click(function(){
                if(this.checked)
                    editor.setValue(window.uglify(editor.getValue(), {}));
                else
                    editor.setValue(oldval);
                tval = editor.getValue();
            });
            $('#nodeps').click(function(){
                if(this.checked)
                    editor.setValue(js_beautify(fp2) + '\n' + oldval, {});
                else
                    editor.setValue(oldval + '\n' + fpload);
                tval = editor.getValue();
            });
            $('#default').click(function(){
                editor.setValue(oldval);
                tval = editor.getValue();
            });
            var t = "", a = "" , a2 = "";
            $('#token').on('input propertychange', function(e){
                t = e.target.value;
            });
            $('#address').on('input propertychange', function(e){
                a = e.target.value;
            });
            $('#replace').click(function(e){
                editor.setValue(tval.replace('{address}', a).replace('{token}', t));
            });
            $('#address2').on('input propertychange', function(e){
                a2 = e.target.value;
            });
            $('#replace2').click(function(e){
                $("#script_utils").html($("#script_utils").html().replace('{address}', a2));
            });
        });
    </script>
@endsection