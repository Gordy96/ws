@extends('admin.layout')
@section('body')
    <div class="notifications" style="position:fixed;top:0px;right:0px;max-width:600px;z-index:99"></div>
    <div class="panel">
        <div class="panel">
        <p class="panel-heading">
            Active connections:
        </p>
        <div class="panel-block">
            <p>
                {{\App\Variable::VarGet('connections')}}
            </p>
        </div>
        <div class="panel-block">
            <table class="table is-bordered is-striped is-narrow is-fullwidth">
                <thead>
                <tr>
                    <td>Count:</td>
                    <td>Country:</td>
                </tr>
                </thead>
                <tbody>
                @foreach(DB::table('clients')
                 ->where('active',true)
                 ->select('country', DB::raw('count(*) as total'))
                 ->groupBy('country')
                 ->get() as $item)
                    <tr>
                        <td>{{$item->total}}</td>
                        <td>{{config('condition.countries.' . $item->country,config('condition.defaults.country'))}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('script')

@endsection