<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        body {
            font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Helvetica,Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
            height: 100%;
        }
        .button.is-success {
            background-color: #2EB398;
        }
        .nav.is-dark {
            background-color: #232B2D;
            color: #F6F7F7;
        }
        .nav.is-dark .nav-item a, .nav.is-dark a.nav-item {
            color: #F6F7F7;
        }
        .nav.is-dark .nav-item a.button.is-default {
            color: #F6F7F7;
            background-color: transparent;
            border-width: 2px;
        }
        .aside {
            background-color: #354052;
            margin-right: -10px;
        }
        .aside .main {
            padding: 20px;
            color: #6F7B7E;
        }
        .aside .title {
            color: #6F7B7E;
            font-size: 12px;
            font-weight: bold;
            text-transform: uppercase;
        }
        .aside .main .item {
            display: block;
            padding: 10px 0;
            color: #6F7B7E;
        }
        .aside .main .item.active {
            color: #F6F7F7;
        }
        .aside .main .icon {
            font-size: 19px;
            padding-right: 10px;
        }
        .aside .main .name {
            font-size: 16px;
            font-weight: bold;
        }
        .admin-panel {
            padding-bottom: 0;
        }
        .content {
            display: block;
            background-color: #fff;
            padding: 40px 20px;
        }
        .hero .tabs ul {
            border-bottom: 1px solid #d3d6db;
        }
        .alert {
            padding: .75rem 1.25rem;
            margin-bottom: 1rem;
            border: 1px solid transparent;
            border-radius: .25rem;
        }
        .alert-warning {
            background-color: #fcf8e3;
            border-color: #faf2cc;
            color: #8a6d3b;
        }
        .alert-danger {
            background-color: #f4645f;
            border-color: #faf2cc;
            color: #FFFFFF;
        }
        @yield('style')
    </style>
</head>
<body>
<div class="columns">
    <aside class="column is-2 aside hero is-fullheight is-hidden-mobile">
        <div>
            <div class="main">
                <div class="title">MENU</div>
                <a href="/control/conditions" class="item {{ strpos(Request::url(), '/conditions') !== false ? 'active' : ''}}"><span class="icon"><i class="fa fa-home"></i></span><span class="name">Conditions</span></a>
                <a href="/control/tasks" class="item {{ strpos(Request::url(), '/tasks') !== false ? 'active' : ''}}"><span class="icon"><i class="fa fa-map-marker"></i></span><span class="name">Tasks</span></a>
                <a href="/control/connections" class="item {{ strpos(Request::url(), '/connections') !== false ? 'active' : ''}}"><span class="icon"><i class="fa fa-list-alt"></i></span><span class="name">Connections</span></a>
                <a href="/control/statistics" class="item {{ strpos(Request::url(), '/statistics') !== false  ? 'active' : ''}}"><span class="icon"><i class="fa fa-th-list"></i></span><span class="name">Statistics</span></a>
                <a href="/control/export" class="item {{ strpos(Request::url(), '/export') !== false  ? 'active' : ''}}"><span class="icon"><i class="fa fa-jsfiddle"></i></span><span class="name">Script export</span></a>
                {{--<a href="/control/frames" class="item {{ strpos(Request::url(), '/frames') !== false  ? 'active' : ''}}"><span class="icon"><i class="fa fa-jsfiddle"></i></span><span class="name">Frames</span></a>--}}
                <a href="/control/clients" class="item {{ strpos(Request::url(), '/clients') !== false  ? 'active' : ''}}"><span class="icon"><i class="fa fa-users"></i></span><span class="name">Active connections</span></a>
                <a href="/logout" class="item"><span class="icon"><i class="fa fa-sign-out"></i></span><span class="name">logout</span></a>
            </div>
        </div>
    </aside>
    <div class="column is-10 admin-panel">
        @yield('body')
    </div>
</div>
<script type="application/javascript" src="{{asset('js/admin/admin.js')}}"></script>
@yield('script')
</body>
</html>
