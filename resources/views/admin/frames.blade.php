@extends('layout')
@section('body')
    <div class="notifications" style="position:fixed;top:0px;right:0px;max-width:600px;z-index:99"></div>
    <div class="panel">
        <div class="panel-block">
            <div class="control">
                <div class="field">
                    <input id="address" type="text" value="/get/img"/>
                </div>
                <div class="field">
                    <input id="submit" type="button" value="Go"/>
                </div>
            </div>
            <div class="control">
                <div class="field">
                    <iframe id="outer" style="width: 728px;height: 90px; overflow: hidden;margin:0;padding:0;border:none" scrolling="no"></iframe>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            $("#submit").click(function(){
                var fields = {
                    address: $("#address").val(),
                    code: $("#code").val()
                };
                $("#outer").attr('src', fields.address);
            })
        })
    </script>
@endsection