@extends('admin.layout')
@section('body')
    <div class="notifications" style="position:fixed;top:0px;right:0px;max-width:600px;z-index:99"></div>
    <div class="panel" style="overflow-x: scroll; overflow-y: scroll">
        <p class="panel-heading">
            Active connections:
        </p>
        <div class="panel-block">
            @if($active)
                <a class="button button-red" href="/control/clients">Show all</a>
            @else
                <a class="button button-blue" href="/control/clients/active">Show active</a>
            @endif
        </div>
        <div class="panel-block">
            <table class="table is-bordered is-striped is-narrow is-fullwidth">
                <thead>
                <tr>
                    <td>Hash</td>
                    <td>Country</td>
                    <td>Browser</td>
                    <td>Platform</td>
                    <td>Accept-Language</td>
                    <td>Task</td>
                    <td>Session start</td>
                    <td>Session end</td>
                    <td>Session duration</td>
                </tr>
                </thead>
                <tbody>
                @foreach($clients as $item)
                    <tr>
                        <td>{{$item->hash}}</td>
                        <td>{{config('condition.countries.' . $item->country,config('condition.defaults.country'))}}</td>
                        <td>{{$item->browser}}</td>
                        <td>{{$item->platform}}</td>
                        <td>{{$item->accept_language}}</td>
                        <td>{{$item->task ? $item->task->name : ''}}</td>
                        <td>{{$item->session_start}}</td>
                        <td>{{$item->session_end}}</td>
                        <td>{{$item->session_duration}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('script')

@endsection