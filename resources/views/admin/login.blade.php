<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        body {
            font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Helvetica,Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
            height: 100%;
        }
        .button.is-success {
            background-color: #2EB398;
        }
        .nav.is-dark {
            background-color: #232B2D;
            color: #F6F7F7;
        }
        .nav.is-dark .nav-item a, .nav.is-dark a.nav-item {
            color: #F6F7F7;
        }
        .nav.is-dark .nav-item a.button.is-default {
            color: #F6F7F7;
            background-color: transparent;
            border-width: 2px;
        }
        .aside {
            background-color: #354052;
            margin-right: -10px;
        }
        .aside .main {
            padding: 40px;
            color: #6F7B7E;
        }
        .aside .title {
            color: #6F7B7E;
            font-size: 12px;
            font-weight: bold;
            text-transform: uppercase;
        }
        .aside .main .item {
            display: block;
            padding: 10px 0;
            color: #6F7B7E;
        }
        .aside .main .item.active {
            color: #F6F7F7;
        }
        .aside .main .icon {
            font-size: 19px;
            padding-right: 30px;
        }
        .aside .main .name {
            font-size: 16px;
            font-weight: bold;
        }
        .admin-panel {
            padding-bottom: 0;
        }
        .content {
            display: block;
            background-color: #fff;
            padding: 40px 20px;
        }
        .hero .tabs ul {
            border-bottom: 1px solid #d3d6db;
        }
    </style>
</head>
<body>
<div class="column is-4 is-offset-4">
    <section class="hero is-fullheight">
        <div class="hero-body">
            <div class="container">
                <div class="columns">
                    <div class="column is-8 is-offset-2">
                        <div class="login-form">
                            <form method="post">
                                <p class="control has-icon has-icon-right">
                                    <input class="input email-input" type="text" placeholder="jsmith@example.org" name="email">
                                    <span class="icon user">
                                    <i class="fa fa-user"></i>
                                </span>
                                </p>
                                <p class="control has-icon has-icon-right">
                                    <input class="input password-input" type="password" placeholder="●●●●●●●" name="password">
                                    <span class="icon user">
                                    <i class="fa fa-lock"></i>
                                </span>
                                </p>
                                <p class="control login">
                                    <button type="submit" class="button is-success is-outlined is-large is-fullwidth">Login</button>
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
</body>
</html>
