@extends('admin.layout')
@section('body')
        <div class="notifications" style="position:fixed;top:0px;right:0px;max-width:600px;z-index:99"></div>
        <div class="panel">
            <p class="panel-heading">
                Condition:
            </p>
            <div class="panel-block">
                <div class="control">
                    <div class="field">
                        <label class="label">Condition for editing</label>
                    </div>
                    <div class="field has-addons">
                        <div class="control is-expanded">
                            <div class="is-fullwidth">
                                <input type="text" name="condition" class="input" list="conditions"/>
                                <datalist id="conditions">
                                    <label>
                                        <select name="condition">
                                            @foreach(\App\Condition::all(['id','name']) as $condition)
                                                <option value="{{$condition->id}}">{{$condition->name}}</option>
                                            @endforeach
                                        </select>
                                    </label>
                                </datalist>
                            </div>
                        </div>
                        <div class="control">
                            <button type="submit" id="edit" class="button is-primary">Edit</button>
                            <button type="submit" id="delete" class="button is-primary">Delete</button>
                            <button type="submit" id="clear" class="button is-primary">Clear</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-block">
                <form method="post" action="/control/api/set/condition" id="main" class="control">
                    <div class="field">
                        <label class="label">Name</label>
                        <div class="control">
                            <input name="name" class="input" type="text">
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">Country</label>
                        <div class="control">
                            <input type="text" name="country" class="input" list="countries"/>
                            <datalist id="countries">
                                <label>
                                    <select name="country">
                                        @foreach($config['countries'] as $iso => $name)
                                            <option value="{{$iso}}">{{$name}}</option>
                                        @endforeach
                                    </select>
                                </label>
                            </datalist>
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">Platform</label>
                        <div class="control">
                            <input type="text" name="platform" class="input" list="platforms"/>
                            <datalist id="platforms">
                                <label>
                                    <select name="platform">
                                        @foreach($config['platforms'] as $platform)
                                            <option value="{{str_ireplace(" ", "_", strtolower($platform))}}">{{$platform}}</option>
                                        @endforeach
                                    </select>
                                </label>
                            </datalist>
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">Browser</label>
                        <div class="control">
                            <input type="text" name="browser" class="input" list="browsers"/>
                            <datalist id="browsers">
                                <label>
                                    <select name="browser">
                                        @foreach($config['browsers'] as $platform)
                                            <option value="{{str_ireplace(" ", "_", strtolower($platform))}}">{{$platform}}</option>
                                        @endforeach
                                    </select>
                                </label>
                            </datalist>
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">Accept-Language</label>
                        <div class="control">
                            <input name="accept_language" class="input" type="text">
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">Socials</label>
                        <div class="control">
                            <label class="checkbox">
                                <input type="checkbox" name="google">
                                Google
                            </label>
                        </div>
                        <div class="control">
                            <label class="checkbox">
                                <input type="checkbox" name="facebook">
                                Facebook
                            </label>
                        </div>
                        <div class="control">
                            <label class="checkbox">
                                <input type="checkbox" name="instagram">
                                Instabram
                            </label>
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">Token</label>
                        <div class="control">
                            <input name="token" class="input" type="text">
                        </div>
                    </div>
                    <input name="id" class="input" type="hidden">
                    <input class="button is-primary" id="confirm" value="Create" type="submit">
                </form>
            </div>
        </div>
@endsection
@section('script')
<script>
$(document).ready(function(){
    $('#delete').on('click', function(){
        var val = $('input[name=condition]').val();
        if(val.length){
            axios.get('/control/api/delete/condition/' + val, {}).then(function(response){
                appendNotification(response.data.type, response.data.error ? 'Error' : 'Ok', response.data.error);
                if(!response.data.error){
                    $('select[name=condition] > option[value=' + val + ']').remove();
                    $('#clear').click();
                }
            }).catch(function(e){
                appendNotification('danger', 'Error', 'No such entry');
            });
        }
    });
     $('#edit').on('click', function(){
         var val = $('input[name=condition]').val();
         if(val.length){
             axios.get('/control/api/get/condition', {
                 params:{
                     con: val
                 }
             }).then(function(response){
                 Object.keys(response.data).forEach(function(element){
                     var elem = $('input[name=' + element + ']');
                     if(elem.length){
                         debugger;
                         if(elem[0].type === 'checkbox')
                             elem.attr('checked', response.data[element] == 1 ? true : false);
                         else
                            elem.val(response.data[element]);
                     }
                 });
                 $("#confirm").val('Update');
             });
         }
     });
    $('input[name=condition]').on('input',function(){
        var val = $('input[name=condition]').val();
        if(!val.length){
            $('input[name]').val('');
            $("#confirm").val('Create');
        }
    });
    $('#clear').on('click',function(){
        $('input[name]').val('');
        $("#confirm").val('Create');
    });
    $('#main').submit(function(){
        var fields = {};
        $(this).find('input[name]').each(function(){
            if(this.type === 'checkbox'){
                if(this.checked)
                    fields[this.name] = this.checked;
            }
            else
                fields[this.name] = this.value;
        });
        axios.post(this.action, fields).then(function(response){
            appendNotification(response.data.type, response.data.error ? 'Error' : 'Ok', response.data.error);
            if(!response.data.error && response.data.data){
                $('select[name=condition]').append('<option value="' + response.data.data.id + '">' + response.data.data.name + '</option>')
            }
        });
        return false;
    });
});
</script>
@endsection