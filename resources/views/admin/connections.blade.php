@extends('admin.layout')
@section('style')
    select[name=tasks] {
        height: unset;
        padding: 0 !important;
    }
    select[name=tasks] option {
        padding: .5em 1em;
    }
@endsection
@section('body')
    <div class="notifications" style="position:fixed;top:0px;right:0px;max-width:600px;z-index:99"></div>
    <div class="panel">
        <div class="panel-heading">
            <label class="label">Connections</label>
        </div>
        <div class="panel-block">
            <div class="control is-expanded">
                <div class="select is-multiple is-fullwidth">
                    <div class="field">
                        <select name="tasks" size="8">
                            @foreach(\App\Task::all(['id','name']) as $task)
                                <option value="{{$task->id}}">{{$task->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="control is-expanded">
                <div class="select is-multiple is-fullwidth">
                    <div class="field">
                        <select name="conditions" multiple size="8">
                            @foreach(\App\Condition::all(['id','name']) as $condition)
                                <option value="{{$condition->id}}">{{$condition->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-block">
            <div class="control is-expanded">
                <div class="field">
                    <input class="button is-primary" id="apply" value="Apply" type="button">
                </div>
                <div class="field">
                    <input class="button is-primary" id="reset" value="Reset *ALL*" type="button">
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            $('select[name=tasks]').on('change', function(){
                var val = $('select[name=tasks] option:selected').val();
                if(val.length){
                    axios.get('/control/api/get/task', {
                        params:{
                            task: val
                        }
                    }).then(function(response){
                        $("select[name=conditions] option").each(function(){
                            this.selected = false;
                        });
                        response.data.conditions.forEach(function(element){
                            $("select[name=conditions] option[value=" + element.id + "]").prop('selected', true);
                        });
                    }).catch(function(e){
                        appendNotification('danger', 'Error', 'No such entry');
                    });
                }
            });
            $('#apply').on('click', function(){
                var id = $('select[name=tasks] option:selected').val();
                var selected = [];
                $('select[name=conditions] option:selected').each(function(){
                    selected.push(this.value);
                });
                axios.post('/control/api/sync/task/condition', {
                    id: id,
                    conditions: selected
                }).then(function(response){
                    appendNotification(response.data.type, response.data.error ? 'Error' : 'Ok', response.data.error);

                });
            });
            $('#reset').on('click', function(){
                axios.get('/control/api/reset/connections').then(function(response){
                    appendNotification(response.data.type, response.data.error ? 'Error' : 'Ok', response.data.error);
                });
            });
        });
    </script>
@endsection