require('./bootstrap');


window.appendNotification = function(type, title, text){
    var template = '<div class="notification is-{{type}}"><button class="delete" onclick="((this).parentNode.remove())"></button><strong>{{title}}</strong><br><small>{{text}}</small></div>';
    var vars = {
        text: text ? text : '',
        title: title,
        type: type
    };
    template = template.replace(/\{\{(.+?)\}\}/gi, function(match,p1){
        return vars[p1];
    });
    var tn = $(template);
    $('.notifications').append(function(){
        var t = tn.clone();
        t.delay(2000).queue(function(){
            $(this).find('.delete').click();
            $(this).dequeue();
        });
        return t;
    });

};

var default_options = {
    parse: {
        bare_returns     : false,
        expression       : false,
        filename         : null,
        html5_comments   : true,
        shebang          : true,
        strict           : false,
        toplevel         : null
    },
    compress: {
        booleans         : true,
        cascade          : true,
        collapse_vars    : true,
        comparisons      : true,
        conditionals     : true,
        dead_code        : true,
        drop_console     : false,
        drop_debugger    : true,
        evaluate         : true,
        expression       : false,
        global_defs      : {},
        hoist_funs       : true,
        hoist_vars       : false,
        ie8              : false,
        if_return        : true,
        inline           : true,
        join_vars        : true,
        keep_fargs       : true,
        keep_fnames      : false,
        keep_infinity    : false,
        loops            : true,
        negate_iife      : true,
        passes           : 1,
        properties       : true,
        pure_getters     : false,
        pure_funcs       : null,
        reduce_vars      : true,
        sequences        : true,
        side_effects     : true,
        switches         : true,
        top_retain       : null,
        typeofs          : true,
        unsafe           : false,
        unsafe_comps     : false,
        unsafe_Func      : false,
        unsafe_math      : false,
        unsafe_proto     : false,
        unsafe_regexp    : false,
        unused           : true,
        warnings         : false
    },
    output: {
        ascii_only       : false,
        beautify         : false,
        bracketize       : false,
        comments         : false,
        ie8              : false,
        indent_level     : 4,
        indent_start     : 0,
        inline_script    : true,
        keep_quoted_props: false,
        max_line_len     : false,
        preamble         : null,
        preserve_line    : false,
        quote_keys       : false,
        quote_style      : 0,
        semicolons       : true,
        shebang          : true,
        source_map       : null,
        webkit           : false,
        width            : 80,
        wrap_iife        : false,
        comments         : /@license|@preserve|^!/
    }
};
window.uglify = function(code, options) {
    // Create copies of the options
    var parse_options = defaults({}, options.parse);
    var compress_options = defaults({}, options.compress);
    var output_options = defaults({}, options.output);

    parse_options = defaults(parse_options, default_options.parse, true);
    compress_options = defaults(compress_options, default_options.compress, true);
    output_options = defaults(output_options, default_options.output, true);

    // 1. Parse
    var toplevel_ast = parse(code, parse_options);
    toplevel_ast.figure_out_scope();

    // 2. Compress
    var compressor = new Compressor(compress_options);
    var compressed_ast = toplevel_ast.transform(compressor);

    // 3. Mangle
    compressed_ast.figure_out_scope();
    compressed_ast.compute_char_frequency();
    compressed_ast.mangle_names();

    // 4. Generate output
    code = compressed_ast.print_to_string(output_options);

    return code;
}